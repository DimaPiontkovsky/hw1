import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MultiplyOrAddTest {
    private static MultiplyOrAdd moaTest;

    @BeforeAll
    public static void setup(){
        moaTest = new MultiplyOrAdd();
    }

    @AfterAll
    public static void close(){
        moaTest = null;
    }

    @Test
    public void test_M_1(){
        assertEquals(9,moaTest.multiplyOrAdd(3,3));

    }
    @Test
    public void test_M_2(){
        assertEquals(6,moaTest.multiplyOrAdd(2,4));

    }

}
