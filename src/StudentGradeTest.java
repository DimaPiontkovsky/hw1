import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StudentGradeTest {
    private static StudentGrade sgTest;

    @BeforeAll
    public static void setup(){
        sgTest = new StudentGrade();
    }
    @AfterAll
    public static void close(){
        sgTest = null;
    }
    @Test
    public void test_F_1(){
        assertEquals("F",sgTest.studentGrade(2));
    }
    @Test
    public void test_F_2(){
        assertEquals("F",sgTest.studentGrade(7));
    }
    @Test
    public void test_F_3(){
        assertEquals("F",sgTest.studentGrade(19));
    }
    @Test
    public void test_E_1(){
        assertEquals("E",sgTest.studentGrade(23));
    }
    @Test
    public void test_E_2(){
        assertEquals("E",sgTest.studentGrade(28));
    }
    @Test
    public void test_E_3(){
        assertEquals("E",sgTest.studentGrade(39));
    }
    @Test
    public void test_D_1(){
        assertEquals("D",sgTest.studentGrade(40));
    }
    @Test
    public void test_D_2(){
        assertEquals("D",sgTest.studentGrade(52));
    }
    @Test
    public void test_D_3(){
        assertEquals("D",sgTest.studentGrade(59));
    }
    @Test
    public void test_C_1(){
        assertEquals("C",sgTest.studentGrade(60));
    }
    @Test
    public void test_C_2(){
        assertEquals("C",sgTest.studentGrade(70));
    }
    @Test
    public void test_C_3(){
        assertEquals("C",sgTest.studentGrade(74));
    }
    @Test
    public void test_B_1(){
        assertEquals("B",sgTest.studentGrade(75));
    }
    @Test
    public void test_B_2(){
        assertEquals("B",sgTest.studentGrade(80));
    }
    @Test
    public void test_B_3(){
        assertEquals("B",sgTest.studentGrade(89));
    }
    @Test
    public void test_A_1(){
        assertEquals("A",sgTest.studentGrade(90));
    }
    @Test
    public void test_A_2(){
        assertEquals("A",sgTest.studentGrade(95));
    }
    @Test
    public void test_A_3(){
        assertEquals("A",sgTest.studentGrade(100));
    }
    @Test
    public void test_Ex_1(){
        assertEquals("False",sgTest.studentGrade(-1));
    }
    @Test
    public void test_Ex_2(){
        assertEquals("False",sgTest.studentGrade(101));
    }
}
