public class StudentGrade {
    public static String studentGrade(int a) {


        if (a >= 0 && a <= 19)
            return "F";
        else if (a >= 20 && a <= 39)
            return "E";
        else if (a >= 40 && a <= 59)
            return "D";
        else if (a >= 60 && a <= 74)
            return "C";
        else if (a >= 75 && a <= 89)
            return "B";
        else if (a >= 90 && a <= 100)
            return "A";
        else
            return "False";
    }
}
