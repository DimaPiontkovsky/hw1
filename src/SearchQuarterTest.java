import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SearchQuarterTest {
    private static SearchQuarter sqTest;

    @BeforeAll
    public static void setup() {
        sqTest = new SearchQuarter();
    }

    @AfterAll
    public static void close() {
        sqTest = null;
    }

    @Test
    public void test_S_1() {
        assertEquals(1, sqTest.searchQuarter(2, 2));

    }
    @Test
    public void test_S_2() {
        assertEquals(2, sqTest.searchQuarter(-2, 2));

    }
    @Test
    public void test_S_3() {
        assertEquals(3, sqTest.searchQuarter(-2, -2));

    }
    @Test
    public void test_S_4() {
        assertEquals(4, sqTest.searchQuarter(2, -2));

    }
    @Test
    public void test_S_5() {
        assertEquals(0, sqTest.searchQuarter(0, -2));

    }
}
