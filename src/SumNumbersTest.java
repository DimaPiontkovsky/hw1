import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SumNumbersTest {
    private static SumNumbers snTest;

    @BeforeAll
    public static void setup (){
        snTest = new SumNumbers();
    }
    @AfterAll
    public static void close(){
        snTest = null;
    }
    @Test
    public void test_S_1(){
        assertEquals(49,2450,snTest.sumNumbers(49, 2450));


    }
}
