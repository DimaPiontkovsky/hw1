import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CalcMaxTest {
    private static CalcMax cmTest;

    @BeforeAll
    public static void setup(){
        cmTest = new CalcMax();
    }
    @AfterAll
    public static void close(){
        cmTest = null;
    }
    @Test
    public void test_C_1(){
        assertEquals(8, cmTest.calcMax(2,2,1));

    }
    @Test
    public void test_C_2(){
        assertEquals(11, cmTest.calcMax(2,2,2));

    }
}
